//					made by jansbury @ jansbury . com 

$(function() {
	var startSize = 40;
	endSize = 200;
	moveSpeed = 1000;
	//append style to the head
	$('head').append("<style>.circle{position:absolute;-webkit-box-shadow:0px -3px 14px darkGray, darkGrey 0px 0px 12px inset;-moz-box-shadow:  0px -3px 14px darkGray, darkGrey 0px 0px 12px inset;-o-box-shadow:  0px -3px 14px darkGray, darkGrey 0px 0px 12px inset;-ms-box-shadow:  0px -3px 14px darkGray, darkGrey 0px 0px 12px inset;box-shadow: 0px -3px 14px darkGray, darkGrey 0px 0px 12px inset;border-radius: 100%;overflow: hidden;}</style>")
	$(document).click(function(e) {
		var startValX = e.pageX - (startSize / 2);
		startValY = e.pageY - (startSize / 2);
		endValX = e.pageX - (endSize / 2);
		endValY = e.pageY - (endSize / 2);
		$('body').append("<div class='circle'></div>");
		$ripple = $('.circle');
		$ripple.css({
			left: startValX,
			top: startValY,
			width: startSize,
			height: startSize,
			opacity: 1,
			zIndex: 100000
		}).animate({
			height: endSize,
			width: endSize,
			left: endValX,
			top: endValY,
			opacity: 0
		}, (moveSpeed), "easeOutExpo", function() {
			$ripple.remove();
		});
	});
});